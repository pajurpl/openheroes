package Database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import logging.ServerLogger;

import Configuration.ConfigurationManager;
import Mob.MobController;
import Mob.MobData;
import World.WMap;

/*
 * MobDAO.class
 * Access the database and parses the data to the needed form for mobs
 */

public class MobDAO {
	private static ServerLogger log = ServerLogger.getInstance();
	
	public static MobData getMobData(int mobID){
		MobData data = null;
		try{
			ResultSet rs = Queries.getMobData(new SQLconnection(false).getConnection(), mobID).executeQuery();
			if (rs.next()){
				data = new MobData();
				data.setLvl(rs.getInt("lvl"));
				data.setAttack(rs.getInt("attack"));
				data.setDefence(rs.getInt("defence"));
				data.setMaxhp(rs.getInt("maxhp"));
				data.setBasexp(rs.getInt("basexp"));
				data.setBasefame(rs.getInt("basefame"));
				data.setAggroRange(rs.getInt("aggroRange"));
				data.setAttackRange(rs.getInt("attackRange"));
				data.setFollowRange(rs.getInt("followRange"));
				data.setMoveRange(rs.getInt("moveRange"));
			}
			else {
				log.warning(MobDAO.class, "Database error: Unable to find mobID " + mobID);
				// System.out.println("ERROR: Unable to find data for mobID " + mobID);
			}
				
			
			
		}catch (SQLException e) {
			log.logMessage(Level.SEVERE, MobDAO.class, e.getMessage());
		}
		catch (Exception e) {
			log.logMessage(Level.SEVERE, MobDAO.class, e.getMessage());
		}
		return data;
	}
	public static void initMobs(){
		int mobid, count, pool;
		int []data = new int[]{0,0,0,0,0,0,0};
		pool = ConfigurationManager.getConf("world").getIntVar("mobUIDPool");
		try{
			ResultSet rs = Queries.getMobs(new SQLconnection(false).getConnection()).executeQuery();
			while(rs.next()){
				mobid = rs.getInt("mobType");
				count = rs.getInt("spawnCount");
				data[0] = rs.getInt("map");
				data[1] = rs.getInt("spawnX");
				data[2] = rs.getInt("spawnY");
				data[3] = rs.getInt("spawnRadius");
				data[4] = rs.getInt("waypointCount");
				data[5] = rs.getInt("waypointHop");
				data[6] = rs.getInt("respawnTime");
				System.out.println("Creating controller with x: " + data[1] + " y: " + data[2]);
				MobController run = new MobController(mobid, count, pool, data);
				WMap.getInstance().getGrid(data[0]).getThreadPool().executeProcess(run);
				pool += count;
			}
		} catch (SQLException e){
			log.logMessage(Level.SEVERE, MobDAO.class, e.getMessage());
		} catch (Exception e){
			log.logMessage(Level.SEVERE, MobDAO.class, e.getMessage());
		}
	}

}
