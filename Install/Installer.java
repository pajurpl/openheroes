package Install;

import java.io.Console;
import java.io.File;
import java.sql.Connection;
import java.sql.Driver;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import logging.ServerLogger;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import Configuration.Configuration;
import Configuration.ConfigurationManager;
import Configuration.XMLParser;
import Database.InstallDAO;
import Database.RuntimeDriverLoader;
import Database.SQLconnection;

public class Installer {
        private Configuration conf;
        private Console con;
        private boolean confOnly;
        private boolean noConf;
        private String filename;
        private InstallDAO dao;
        
        public Installer(boolean only, boolean no, String file){
                ConfigurationManager.initInstall();
                this.conf = ConfigurationManager.getConf("GameServer");
                this.con = System.console();
                this.confOnly = only;
                this.noConf = no;
                this.filename = file;
                ServerLogger.getInstance().info(this, "Installation starting");
                this.dao = InstallDAO.getInstance();
                this.start();
                 
        }
        private void start(){
                String in = this.conf.getVar("driver");
                System.out.println("Starting OpenHeroes install process");
                System.out.print("Testing MySQL driver.. ");
                        while(!this.driverTest(in)){ 
                                System.out.print("Enter location of driver: ");
                                in = this.con.readLine();
                                this.conf.setVar("driver", in);
                        }
                System.out.println("Success");
                boolean b = false;
                while(!b){
                        System.out.print("Enter your MySQL server ip address:");
                        in = this.con.readLine();
                        this.conf.setVar("host", in);
                        System.out.print("Enter your MySQL username:");
                        in = this.con.readLine();
                        this.conf.setVar("username", in);
                        System.out.print("Enter your MySQL password:");
                        in = new String(this.con.readPassword());
                        this.conf.setVar("password", in);
                        System.out.print("Enter name of your database:");
                        in = this.con.readLine();
                        this.conf.setVar("db", in);
        
                        b = this.dao.isEmpty();
                        if (!b){
                                System.out.println("Try again? [Y/N]");
                                in = this.con.readLine();
                                if (!in.toLowerCase().trim().contentEquals("y")){ System.out.println("Terminating, install failed"); return; }
                        }
                }
                System.out.println("Connection successfully established");
                if (!this.confOnly){
                        System.out.println("Checking for previous installs...");
                        
                        boolean bol[] = this.checkTables();
                
                        System.out.print("Creating tables..");
                        if (bol[0]) if (!this.dao.createAccountTable()) { System.out.println("Failed to create table \"accounts\"... terminating, install failed"); return; }
                        if (bol[3]) if (!this.dao.createMapTable()){ System.out.println("Failed to create table \"maps\".. terminating, install failed"); return; }
                        if (bol[3]) if (!this.dao.createItemsTable()){ System.out.println("Failed to create table \"items\".. terminating, install failed"); return; }
                        if (bol[1]) if (!this.dao.createCharacterTable()) { System.out.println("Failed to create table \"character\".. terminating, install failed"); return; }
                        if (bol[4]) if (!this.dao.createMobDataTable()) { System.out.println("Failed to create table \"mobData\".. terminating, install failed"); return; }
                        if (bol[5]) if (!this.dao.createMobsTable()) { System.out.println("Failed to create table \"mobs\".. terminating, install failed"); return; }
                        if (bol[6]) if (!this.dao.createEquipmentTable()) { System.out.println("Failed to create table \"mobs\".. terminating, install failed"); return; }
                        System.out.println("Done");
                
                        System.out.print("Creating map entries. ");
                        this.createMaps();
                        System.out.println("Done");
                        System.out.print("Creating the default user account..");
                        this.createDefaultAccount();
                        System.out.println("Creating mobData entries");
                        this.createMobData();
                        System.out.println("done");
                }
                if (!this.noConf)
                {
                        System.out.print("Generation configuration file..");
                        this.generateServerConf();
                }
                System.out.println("Installation complete");
        }
        private void createMobData() {
        	XMLParser par = new XMLParser("Data/MobData.xml");
        	Element root = par.getRoot();
			Element el = null;
			if (par.getElementName(root) == "MobData"){
				int id, lvl, att, deff, hp;
				int basexp = 10;
				int basefame = 1;
				int aggro = 30;
				int attrange = 5;
				int follow = 100;
				int move = 10000;
				int entries = par.getNodeCount(root, "mob");
				System.out.print("Found " + entries + " entries, processing");
				Connection con = new SQLconnection().getConnection();
				
				for (int i=0; i< entries; i++){
					el = par.getElement(root, "mob", i);
					id = par.getIntValue(el, "id");
					lvl = par.getIntValue(el, "lvl");
					att = par.getIntValue(el, "attack");
					deff = par.getIntValue(el, "defence");
					hp = par.getIntValue(el, "hp");
					InstallDAO.getInstance().createMobDataEntry(con, id, lvl, att, deff, hp, basexp, basefame, aggro, attrange, follow, move);
					
					if (i > 10) { if ( (i % (i/10)) == 0){ System.out.print("."); } }
					
				}
			}
			
		}
		private void createDefaultAccount() {
        	this.dao.CreateAccount(1, "localhost", "localhost", 1);
			
		}
		private void createMaps() {
			XMLParser par = new XMLParser("Data/Maps.xml");
            Element root = par.getRoot();
			Element el = null;
			if (par.getElementName(root) == "Maps"){
				int id, x,y, asize,gsize, pool;
				String name;
				
				int entries = par.getNodeCount(root, "Map");
				System.out.print("Found " + entries + " entries, processing");
				
				for (int i=0; i< entries; i++){
					el = par.getElement(root, "Map", i);
					id = par.getIntValue(el, "id");
					name = par.getTextValue(el, "name");
					x = par.getIntValue(el, "mapx");
					y = par.getIntValue(el, "mapy");
					asize = par.getIntValue(el, "areaSize");
					gsize = par.getIntValue(el, "gridSize");
					pool = par.getIntValue(el, "mobPool");
					this.dao.addMap(id, name, gsize, asize, x, y , pool);
					
					System.out.print(".");
					
				}
			}
		}
		private boolean[] checkTables(){
        	boolean b[] = new boolean[]{false, false, false, false, false, false, false};
        	String []tables = new String[]{"accounts", "characters", "items", "maps", "mobs", "mobData", "equipments"};
        	String in;
        	
        	for (int i =0; i < tables.length; i++){
        		if (this.dao.tableExists(tables[i])){
        			System.out.println("Table \""+tables[i]+"\" already exists");
        			System.out.println("Do you wish to overwrite?[Y/N] (Notice: All previous data will be lost)");
        			in = this.con.readLine();
        			if (in.toLowerCase().trim().contentEquals("y")){ this.dao.dropTable(i); b[i] = true;}
        			else { System.out.println("Table \""+tables[i]+"\" is unchanged"); }
            	}
        		else{
        			b[i] = true;
        		}
        	}
        	return b;
        }
        private boolean driverTest(String path){
                Driver t = new RuntimeDriverLoader().loadDriver(path, false);
                if (t != null) return true;
                return false;
        }
        private void generateServerConf(){
                DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = null;
                try {
                        docBuilder = docFactory.newDocumentBuilder();
                } catch (ParserConfigurationException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                }
 
                // root element
                Document doc = docBuilder.newDocument();
                Element rootElement = doc.createElement("GameServer");
                doc.appendChild(rootElement);
                
                //database elements
                Element database = doc.createElement("database");
                rootElement.appendChild(database);
 
                Element user = doc.createElement("username");
                user.appendChild(doc.createTextNode(this.conf.getVar("username")));
                database.appendChild(user);
                
                Element password = doc.createElement("password");
                password.appendChild(doc.createTextNode(this.conf.getVar("password")));
                database.appendChild(password);
                
                Element host = doc.createElement("host");
                host.appendChild(doc.createTextNode(this.conf.getVar("host")));
                database.appendChild(host);
                
                Element db = doc.createElement("db");
                db.appendChild(doc.createTextNode(this.conf.getVar("db")));
                database.appendChild(db);
                
                Element driver = doc.createElement("driver");
                driver.appendChild(doc.createTextNode(this.conf.getVar("driver")));
                database.appendChild(driver);
                
                
                //server elements
                Element server = doc.createElement("server");
                rootElement.appendChild(server);
                
                Element port = doc.createElement("port");
                port.appendChild(doc.createTextNode(this.conf.getVar("port")));
                server.appendChild(port);
                
                // World elements
                Element world = doc.createElement("world");
                rootElement.appendChild(server);
                
                Element mobuid = doc.createElement("MobUIDPool");
                mobuid.appendChild(doc.createTextNode("50000"));
                world.appendChild(mobuid);
                
                
                // lobby elements
                Element lobby = doc.createElement("lobby");
                rootElement.appendChild(lobby);
                
                Element cport = doc.createElement("cdpPort");
                cport.appendChild(doc.createTextNode(this.conf.getVar("cdpPort")));
                lobby.appendChild(cport);
                
                Element lport = doc.createElement("lobbyPort");
                lport.appendChild(doc.createTextNode(this.conf.getVar("lobbyPort")));
                lobby.appendChild(lport);
                
                // logging elements
                Element log = doc.createElement("Logging");
                rootElement.appendChild(log);
                
                Element loglevel = doc.createElement("logWriteLevel");
                loglevel.appendChild(doc.createTextNode("warning"));
                log.appendChild(loglevel);
                
                Element logout = doc.createElement("logOutputLevel");
                logout.appendChild(doc.createTextNode("info"));
                log.appendChild(logout);
                
                Element logfile = doc.createElement("logFile");
                logfile.appendChild(doc.createTextNode("Error.log"));
                log.appendChild(logfile);
                
                Element loglob = doc.createElement("logLobby");
                loglob.appendChild(doc.createTextNode("no"));
                log.appendChild(loglob);
                
                
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer;
                try {
                        transformer = transformerFactory.newTransformer();
                
                        DOMSource source = new DOMSource(doc);
                        StreamResult result = new StreamResult(new File(this.filename));
 
                        // Output to console for testing
                        // StreamResult result = new StreamResult(System.out);
 
                        transformer.transform(source, result);
 
                } catch (TransformerConfigurationException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                } catch (TransformerException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                }
                System.out.println("Done!");
                                
        }

}