package item.inventory;

import item.ItemFrame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Inventory {
	private int pages;
	private Map<Integer, ItemFrame> inv = new HashMap<Integer, ItemFrame>();
	private LinkedList<Integer> seq;
	
	public Inventory(int pages){
		this.pages = pages;
		this.seq = new LinkedList<Integer>();
		this.fillSequences();
		
	}
	// make sure all sequences are present
	private void fillSequences(){
		for (int i = 0; i <= 240; i++){
			this.seq.add(new Integer(0));
		}
	}
	// find first free sequence (aka. first that has value of 0)
	private int nextFreeSequence()
	{
		int c = this.seq.indexOf(0);
		System.out.println("New sequence; " + c);
		return c;
	}
	// increase amount of pages in inventory
	public void addPages(int amount){
		if (amount > 0 || amount < 4) this.pages += amount;
	}
	// adds item to inventory only if all needed slots are empty, return true is success, false otherwise
	public boolean addItem(int line, int row, ItemFrame it){
		List<ItemFrame> ls = this.checkBlockingItems(line, row, it);
		if (ls == null){
			return false;
		}
		if (ls.size() == 0 ){
			this.put(line, row, it);
			return true;	
		}
		return false;
	}
	// adds item to inventory and return the item replaced if possible 
	public ItemFrame AddAndReturn(int line, int row, ItemFrame it){
		List<ItemFrame> ls = this.checkBlockingItems(line, row, it);
		ItemFrame  ret = null;
		if (ls == null){
			return ret;
		}
		else if (ls.size() == 1){
			this.put(line, row, it);
			ret = ls.get(0);
			this.removeAll(ret);
		}
		else if (ls.size() == 0){
			this.put(line, row, it);
		}
		return ret;
		
	}
	// removes item from inventory and returns true on success, false otherwise
	public boolean removeItem(int line, int row){
		ItemFrame it = this.inv.get((row*100)+line);
		return this.removeAll(it);
	}
	// removes item from inventory list and returns the deleted object
	public ItemFrame removeAndReturn(int line, int row){
		ItemFrame it = null;
		if (this.inv.containsKey((row*100)+line)){
			it = this.inv.get((row*100)+line);
			this.remove(line, row);
		}
		return it;
	}
	// return item on specific slot, null if no item present
	public ItemFrame getItem(int line, int row) {
		return this.inv.get((row*100)+line);
	}
	// remove item by iterating through all slots
	private void remove(int line, int row) {
		int slot = (row*100) + line;
		ItemFrame it = this.inv.get(slot);
		this.seq.set(this.seq.indexOf(slot), 0);
		if (it != null){
			for (int i = line;  i < (line + it.getWidth()); i++){
				for (int u=row; u < (row + it.getHeight()); u++){
					this.inv.remove(Integer.valueOf((u*100)+i));
				}
			}
		}
	}
	// insert item to all the slots it requires, no boundary checks are performed
	private void put(int line, int row, ItemFrame it) {
		this.seq.set(this.nextFreeSequence(), (row*100) + line);
		for (int i = line;  i < (line + it.getWidth()); i++){
			for (int u=row; u < (row + it.getHeight()); u++){
				this.inv.put(Integer.valueOf((u*100)+i), it);
			}
		}
		
	}
	// remove all instances of Item
	private boolean removeAll(ItemFrame it){
		if (this.inv.containsValue(it)){
			return this.inv.values().removeAll(Collections.singleton(it));
		}
		return false;
	}
	/* check if item will fit in inventory
	 *  Returns list containing all items that are using same slots as tmp
	 */
	private List<ItemFrame> checkBlockingItems(int line, int row, ItemFrame tmp){
		int width = tmp.getWidth();
		List<ItemFrame> ls = new ArrayList<ItemFrame>();
		int height = tmp.getHeight();
		ItemFrame it = null;
		int c = 0;
		 
		// boundary check
		if (!this.checkWlimit(row, width)) return null;
		if (!this.checkHlimit(line, height)) return null;
		
		// main loop
		for (int i=line; i < (line + height); i++){
			for (int u=row; u < (row + width); u++){
				// if item is in slot add it to list
				if (this.inv.containsKey((u*100)+i)){
					it = this.inv.get((u*100) +i);
					if (!ls.contains(it)) ls.add(c, it);
					c++;
				}
			}
			
		}
		return ls;
	}
	// check if item will fit to inventory by height
	private boolean checkHlimit(int line, int height) {
		if (line + height < 5) return true;
		return false;
	}
	// check if item will fit to inventory by width
	private boolean checkWlimit(int row, int width) {
		if ((this.pages*8) > (row + width)){
			int page = (int)Math.floor(row / 8);
			if ((row+width) <= (page*8+7) && row >= (page*8)){
				return true;
			}
		}
		return false;
	}
	
}
