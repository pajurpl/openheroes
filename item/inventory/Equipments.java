package item.inventory;

import item.EquipableItem;
import item.Item;
import Player.Character;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* Equipment slots in game
0.Cap
1.Necklace
2.Cape
3.Jacket
4.Pants
5.Armor
6.Bracelet
7.Weapon1
8.Weapon2
9.Ring1
10.Ring2
11.Shoes
12.Bird
13.Tablet
14.Fame pad
15.Mount
16.Bead
 */

/*
 * Normal + set slots: 0-6, 9-11		0
 * Ultimate set slots: 0-7, 9-11		1
 * Necky set slots: 1, 6, 9, 10			2
 * Necky set with tab: 1, 6, 9, 10, 13 	3
 * Fame set slots: 1, 5, 6, 9, 10		4
 */

public class Equipments {
	private int hp, mana, stamina;
	private int atk, deff;
	private short []stats =  new short[]{0,0,0,0,0};
	private short []setStats =  new short[]{0,0,0,0,0};
	private float []typeDmg = new float[]{0,0,0,0};
	private float atk_succes, def_success, crit_rate;
	private Map<Integer, Item> equipments = new HashMap<Integer, Item>(16);
	private List<byte[]> equipMappings = new ArrayList<byte[]>();
	private Character owner;
	
	private final byte[] plusSetSlots = new byte[] { 0, 1, 2, 3, 4, 5, 6, 9, 10, 11 }; 			//0
	private final byte[] ultimateSetSlots = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11 }; 	//1
	private final byte[] neckySetSlots = new byte[] { 1, 6, 9, 10 }; 							//2
	private final byte[] neckySetMedTabSlots = new byte[] { 1, 6, 9, 10, 13 }; 					//3
	private final byte[] fameSetSlots = new byte[] { 1, 5, 6, 9, 10 }; 							//4
	
	public Equipments(Character owner) {
		this.owner = owner;
		this.equipMappings.add(plusSetSlots);
		this.equipMappings.add(ultimateSetSlots);
		this.equipMappings.add(neckySetSlots);
		this.equipMappings.add(neckySetMedTabSlots);
		this.equipMappings.add(fameSetSlots);
		this.setToZero();
	}
	// NOTE use only for testing purposes
	public Equipments(){
		this.owner = null;
		this.equipMappings.add(plusSetSlots);
		this.equipMappings.add(ultimateSetSlots);
		this.equipMappings.add(neckySetSlots);
		this.equipMappings.add(neckySetMedTabSlots);
		this.equipMappings.add(fameSetSlots);
		this.setToZero();
		
	}
	
	public boolean equip(int slot, EquipableItem it) {
		if(owner.getLevel() >= it.getMinLvl()) {
			short[] meh = owner.getStats();
			short[] req = it.getStatRequirements();
			for(int i=0;i<meh.length;i++) {
				if(!(meh[i] >= req[i])) {
					return false;
				}
			}
			this.equipments.put(Integer.valueOf(slot), it);
			this.calculateEquipmentStats();
			if(this.isFullSet()) { /*TODO: calculate set stats*/ }
			return true;
		}
		return false;
	}
	
	/*
	 * Quick swapping items - set new item and get old item
	 * Return value: old item or null if none or operation not allowed
	 */
	public Item equipAndGetItem(int slot, EquipableItem it) {
		Item old = this.equipments.get(Integer.valueOf(slot));
		if(owner.getLevel() >= it.getMinLvl() && it.getUsableClass()[owner.getCharacterClass()]) {
			short[] meh = owner.getStats();
			short[] req = it.getStatRequirements();
			for(int i=0;i<meh.length;i++) {
				if(!(meh[i] >= req[i])) {
					return null;
				}
			}
			this.equipments.put(Integer.valueOf(slot), it);
			this.calculateEquipmentStats();
			if(this.isFullSet()) { /*TODO: calculate set stats*/ }
			return old;
		}
		return null;
	}
	// NOTE only for testing purposes
	public void testEquip(int slot, EquipableItem it){
		this.equipments.put(slot, it);
		this.calculateEquipmentStats();
	}
	
	/*
	 * test if character wears full set of any type
	 */
	private boolean isFullSet() {
		Item necky = this.equipments.get(Integer.valueOf(1));
		if(necky != null) {
			// int hash = necky.getSetHash();
			// byte[] equipIterSequence = this.equipMappings.get(hash);
			/*
			Item current;
			for(int i=0;i<hash[1];i++) {				
				current = this.equipments.get(Integer.valueOf((int)equipIterSequence[i]));
				if(current != null) {
					byte[] curHash = BitTools.intToByteArray(current.getSetHash());
					if(!(curHash[2] == hash[2] && curHash[3] == hash[3])) {
						return false;
					}
				} else {
					return false;
				}
			}
			*/
		}
		return true;
	}
	
	public Item unequip(int slot) {
		Item it = this.equipments.get(Integer.valueOf(slot));
		if (this.equipments.containsKey(slot)) this.equipments.remove(slot);
		return it;
	}
	
	public Character getOwner() {
		return this.owner;
	}

	public Map<Integer, Item> getEquipments() {
		return equipments;
	}

	public void setEquipments(Map<Integer, Item> equipments) {
		this.equipments = equipments;
	}
	/*  Reset all stats to zero
	 * this should only be called from constructor and calculateEquipmentStats
	 */
	private void setToZero(){
		this.hp = 0;
		this.mana = 0;
		this.stamina = 0;
		this.atk = 0;
		this.deff = 0;
		for (int i=0; i <5; i++) this.stats[i] = 0;
		for (int i=0; i <5; i++) this.setStats[i] = 0;
		for (int i=0; i <4; i++){ this.typeDmg[i] = 0;}
		this.atk_succes = 0;
		this.def_success = 0;
		this.crit_rate = 0;
	}
	// calculate stats given by currently equipped items
	public void calculateEquipmentStats(){
		this.setToZero();
		for (int i=0; i< 16; i++){
			if (this.equipments.containsKey(i) && i != 8){
				EquipableItem it = this.equipments.get(i).getEquipable();
				System.out.println("Item at slot:" + i);
				this.hp += it.getHp();
				this.mana += it.getMana();
				this.atk += it.getAtk();
				this.deff += it.getDef();
				this.crit_rate += it.getCritSucc();
				this.atk_succes += it.getAttSucc();
				this.def_success += it.getDefSucc();
				this.stamina += it.getStamina();
				short []ts = it.getStatBonuses();
				for (int u=0; u<5; u++) this.stats[u] += ts[u];
				this.typeDmg[(int)it.getType()] += it.getTypeDmg();
			}
		}
	}
	public void printEquipStats(){
		System.out.println("Total equipment bonus");
		System.out.println("HP: " + this.hp + " Mana: " + this.mana + " stamina: " + this.stamina);
		System.out.println("Attack: " + this.atk + " Defense: " + this.deff);
		System.out.println("Str: " + this.stats[0] + " int: " + this.stats[1] + " vit: " + this.stats[2]);
		System.out.println("Agi: " + this.stats[3] + " Dex: " + this.stats[4]);
	}

	public int getHp() {
		return hp;
	}

	public int getMana() {
		return mana;
	}

	public int getStamina() {
		return stamina;
	}

	public int getAtk() {
		return atk;
	}

	public int getDeff() {
		return deff;
	}

	public short[] getStats() {
		return stats;
	}

	public short[] getSetStats() {
		return setStats;
	}

	public float getAtk_succes() {
		return atk_succes;
	}

	public float getDef_success() {
		return def_success;
	}

	public float getCrit_rate() {
		return crit_rate;
	}
	public float [] getTypeDmg() {
		return typeDmg;
	}

}
