package item;

public class ConsumableItem extends ItemFrame {
	
	private int maxStackSize;
	
	
	
	public ConsumableItem(int id) { 
		super(id);
	}
	public int getMaxStackSize() {
		return maxStackSize;
	}
	public void setMaxStackSize(int maxStackSize) {
		this.maxStackSize = maxStackSize;
	}
	public ConsumableItem getConsumable(){
		return this;
	}

}
