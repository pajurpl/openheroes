package item;

import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import logging.ServerLogger;

import Player.Character;
import ServerCore.ServerFacade;
import Tools.BitTools;
import World.Grid;
import World.Area;
import World.Location;
import World.OutOfGridException;
import World.WMap;
import World.Waypoint;

public class DroppedItem implements Location{
	private WMap wmap = WMap.getInstance();
	private List<Integer> iniPackets = new ArrayList<Integer>();
	private int Uid; // unique id for grid n such
	private Waypoint location;
	private ItemFrame item;
	private int map;
	private Grid grid;
	private Area area;
	private ServerLogger log = ServerLogger.getInstance();
	
	DroppedItem(ItemFrame it,int map, int uid, Waypoint loc){
		this.Uid = uid;
		this.item = it;
		this.map = map;
		this.location = loc;
		this.joinGameWorld();
	}

	public void setuid(int uid) {
		this.Uid = uid;
	}

	public int getuid() {
		return Uid;
	}
	public Waypoint getLocation() {
		return this.location;
	}
	public void updateEnvironment(List<Integer> players) {
		Iterator<Integer> plIter = this.iniPackets.iterator();
		Integer tmp = null;
		
		while(plIter.hasNext()) {
			tmp = plIter.next();
			if(!players.contains(tmp)) {//if character is no longer in range, remove it
				plIter.remove();	
			}
			else { // remove from need ini list if we already have it on the list
				players.remove(tmp);
			}
			if(!this.wmap.CharacterExists(tmp)) { // remove if not a valid character
				players.remove(tmp);
			} 
		}
		
		this.sendInit(players);
	}
	public void joinGameWorld() {
		this.wmap.addItem(this);
		if (this.wmap.gridExist(map)){
			try {
				this.grid = this.wmap.getGrid(this.map);
				this.area = this.grid.update(this);
			} catch (OutOfGridException e) {
				this.log.warning(this, e.getMessage() + " Somehow, someone, somewhere dropped an item outside grid.");
			}
			this.area.addMember(this);
		}
		else {
			ServerLogger.getInstance().logMessage(Level.SEVERE, this, "Failed to load grid for teim "+this.Uid +" map:" +this.map + ", disconnecting");
		}
	}
	
	/*
	 * item has been picked and this instance is no longer required
	 */
	public void leaveGameWorld() {
		this.area.rmMember(this);
		this.wmap.removeItem(Uid);
		this.iniPackets.clear();
	}
	private void sendInit(List<Integer> sendList) {
		Iterator<Integer> siter = sendList.iterator();
		Integer tmp = null;
		while(siter.hasNext()) {
			tmp = siter.next();
			if (this.wmap.CharacterExists(tmp)){
				Character t = this.wmap.getCharacter(tmp);
				SocketChannel sc = t.GetChannel();
				ServerFacade.getInstance().getConnectionByChannel(sc).addWrite(this.itemSpawnPacket());
				// if (this.vanish.containsKey(tmp)) this.vanish.remove(tmp);
			}
		}
		this.iniPackets.addAll(sendList);
	}
	public byte[] itemSpawnPacket() {
		byte[] item = new byte[56];
		byte[] spawnX = BitTools.floatToByteArray(this.location.getX());
		byte[] spawnY = BitTools.floatToByteArray(this.location.getY());
		byte[] itid = BitTools.intToByteArray(this.item.getId());
		byte[] chid = BitTools.intToByteArray(this.getuid());
		
		item[0] = (byte)item.length;
		item[4] = (byte)0x05;
		item[6] = (byte)0x0e;
		
		for(int i=0;i<4;i++) {
			item[36+i] = spawnX[i];
			item[40+i] = spawnY[i];
			item[20+i] = itid[i];
			item[32+i] = chid[i]; 
		}
		
		return item;
	}

	@Override
	public float getlastknownX() {
		// TODO Auto-generated method stub
		return this.location.getX();
	}

	@Override
	public float getlastknownY() {
		return this.location.getY();
	}

	@Override
	public SocketChannel GetChannel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public short getState() {
		// TODO Auto-generated method stub
		return 0;
	}
	public ItemFrame getItem(){
		return this.item;
	}

	@Override
	public void updateEnvironment(Integer players, boolean add) {
		// TODO Auto-generated method stub
		
	}
}
