package Player;

import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;


public class Player {
	private List<Character> characters;
	private int accountID;
	private int flag;
	private SocketChannel sc;
	private Character activeCharacter;
	
	public Player(int id) {
		this.accountID = id;
		this.characters = new ArrayList<Character>();
	}
	
	public List<Character> getCharacters() {
		return characters;
	}
	public void setCharacters(ArrayList<Character> characters) {
		this.characters.addAll(characters);
	}
	public int getAccountID() {
		return accountID;
	}
	public void setAccountID(int accountID) {
		this.accountID = accountID;
	}
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}

	public SocketChannel getSc() {
		return sc;
	}
	public void setChannel(SocketChannel chan){
		this.sc = chan;
	}

	public Character getActiveCharacter() {
		return activeCharacter;
	}

	public void setActiveCharacter(Character activeCharacter) {
		this.activeCharacter = activeCharacter;
	}
	
	public void addCharacter(Character ch) {
		this.characters.add(ch);
	}
	
	public void removeCharacter(Character ch) {
		this.characters.remove(ch);
	}
	
	public boolean hasActiveCharacter() {
		return (this.activeCharacter == null) ? false : true;
	}
}
