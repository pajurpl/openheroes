package GameServer.GamePackets;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;

import Connections.Connection;
import Database.CharacterDAO;
import Encryption.Decryptor;
import Player.Player;
import Player.PlayerConnection;
import Player.Character;
import Tools.BitTools;

public class SelectedCharacter implements Packet {

	@Override
	public void execute(ByteBuffer buff) {
		// TODO Auto-generated method stub
		
	}

	public byte[] returnWritableByteBuffer(byte[] buffyTheVampireSlayer, Connection con) {
		Character ch = null;
		byte[] decrypted = new byte[(buffyTheVampireSlayer[0] & 0xFF)-8];

		for(int i=0;i<decrypted.length;i++) {
			decrypted[i] = (byte)(buffyTheVampireSlayer[i+8] & 0xFF);
		}
		decrypted = Decryptor.Decrypt(decrypted);
		
		Player polishPlayer = ((PlayerConnection)con).getPlayer();
		if(polishPlayer != null) {
			ch = polishPlayer.getCharacters().get((int)decrypted[0]);
			polishPlayer.setActiveCharacter(ch);
			Character tempMofo = CharacterDAO.loadCharacter(ch.getCharID());
			ch.setCurrentMap(tempMofo.getCurrentMap());
			ch.setX(tempMofo.getlastknownX());
			ch.setY(tempMofo.getlastknownY());

		
		byte[] first = new byte[1452];
		byte[] second = new byte[1452];
		byte[] third = new byte[1452];
		byte[] fourth = new byte[1452];
		
		for(int i=0;i<1452;i++) {
			first[i] = 0x00;
			second[i] = 0x00;
			third[i] = 0x00;
			fourth[i] = 0x00;
		}
		
		byte[] stuffLOL = new byte[] {(byte)0xc0, (byte)0x16, (byte)0x00, (byte)0x00, (byte)0x04, (byte)0x00, (byte)0x01, (byte)0x00, (byte)0x01, (byte)0x00, (byte)0x00, (byte)0x00,  
			(byte)0x00, (byte)0x00, (byte)0x00, (byte)0x01,  
			(byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, 
			(byte)0x64, 
			(byte)0x00, (byte)0x00, (byte)0x00, 
			(byte)0x04, 
			(byte)0x00, (byte)0x00, (byte)0x00, 
			(byte)0x02, (byte)0x05, (byte)0x0a};
		
		byte[] cha = new byte[] {
				(byte)0x0c, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x9d, (byte)0x0f, (byte)0xbf, 
				(byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x50, (byte)0x2e
			};
		
		byte[] coordinates = new byte[8];
		byte[] xCoord = BitTools.floatToByteArray(ch.getlastknownX());
		byte[] yCoord = BitTools.floatToByteArray(ch.getlastknownY());
		byte[] chID = BitTools.intToByteArray(ch.getCharID());
		
		for(int i=0;i<4;i++) {
			stuffLOL[i+12] = chID[i];
			coordinates[i] = xCoord[i];
			coordinates[i+4] = yCoord[i];
		}
		
		stuffLOL[20] = (byte)polishPlayer.getActiveCharacter().getCurrentMap();
		
		for(int i=0;i<stuffLOL.length;i++) {
			first[i] = stuffLOL[i];
		}
		
		for(int i=0;i<coordinates.length;i++) {
			fourth[i+(1452-coordinates.length)] = coordinates[i];
		}
		
		synchronized(con.getWriteBuffer()) {
			con.addWriteButDoNotFlipInterestOps(first);
			con.addWriteButDoNotFlipInterestOps(second);
			con.addWriteButDoNotFlipInterestOps(third);
			con.addWriteButDoNotFlipInterestOps(fourth);
			con.addWriteButDoNotFlipInterestOps(cha);
		}
		con.flipOps(SelectionKey.OP_WRITE);
		polishPlayer.getActiveCharacter().joinGameWorld();
	}
		return null;
	}
	
	
}
