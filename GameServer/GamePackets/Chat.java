package GameServer.GamePackets;

import java.nio.ByteBuffer;

import chat.ChatParser;

import Connections.Connection;
import Encryption.Decryptor;
import Player.Player;
import Player.PlayerConnection;
import Player.Character;


public class Chat implements Packet {

	@Override
	public void execute(ByteBuffer buff) {
		// TODO Auto-generated method stub
		
	}


	public byte[] returnWritableByteBuffer(byte[] buffyTheVampireSlayer, Connection con) {
		byte[] decrypted = new byte[(buffyTheVampireSlayer[0] & 0xFF)-8];
		
		for(int i=0;i<decrypted.length;i++) {
			decrypted[i] = (byte)(buffyTheVampireSlayer[i+8] & 0xFF);
		}
		
		decrypted = Decryptor.Decrypt(decrypted);
		
		Player tmplayer = ((PlayerConnection)con).getPlayer();
		Character current = tmplayer.getActiveCharacter();
		
		
		byte[] name = current.getName().getBytes();
		byte[] chatRelay = new byte[decrypted[19]+44];
		
		chatRelay[0] = (byte)chatRelay.length;
		chatRelay[4] = (byte)0x05;
		chatRelay[6] = (byte)0x07;
		
		for(int i=0;i<name.length;i++) {
			chatRelay[i+20] = name[i];
		}
		
		byte[] stuffz = new byte[] { (byte)0x01, (byte)0xfe, (byte)0x14, (byte)0x08, (byte)0x30, (byte)0x12, (byte)0x0c, (byte)0x00,  
	       (byte)0x00, (byte)0x01, (byte)0x00, (byte)0x00};
	       
		for(int i=0;i<stuffz.length;i++) {
			chatRelay[i+8] = stuffz[i];
		}
		
		byte[] msgbytes = new byte[decrypted[19]];
		
		for(int i=0;i<decrypted[19];i++) {
			chatRelay[i+44] = decrypted[i+23];
			msgbytes[i] = decrypted[i+23];
		}
		
		chatRelay[40] = decrypted[19];
		
		current.sendToMap(chatRelay);
		ChatParser.getInstance().parseAndExecuteChatCommand(new String(msgbytes), con);
		
		return null;
	}

}
