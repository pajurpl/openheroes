package GameServer.GamePackets;

import item.ItemCache;
import item.ItemFrame;

import java.nio.ByteBuffer;

import Connections.Connection;
import Encryption.Decryptor;
import Player.Character;
import Player.PlayerConnection;
import Tools.BitTools;
import World.WMap;

public class Pick implements Packet {
	ItemCache cache = ItemCache.getInstance();

	@Override
	public void execute(ByteBuffer buff) {
		// TODO Auto-generated method stub
		
	}


	public byte[] returnWritableByteBuffer(byte[] buffyTheVampireSlayer, Connection con) {
		Character cur = ((PlayerConnection)con).getActiveCharacter();
		byte[] chid = BitTools.intToByteArray(cur.getCharID());
		byte[] decrypted = new byte[(buffyTheVampireSlayer[0] & 0xFF)-8];
		
		for(int i=0;i<decrypted.length;i++) {
			decrypted[i] = (byte)(buffyTheVampireSlayer[i+8] & 0xFF);
		}
		
		decrypted = Decryptor.Decrypt(decrypted);
		
		System.out.println("Handling pick");
		byte[] pick = new byte[40];
		byte[] uid = new byte[4];
		
		for(int i=0;i<4;i++) {
			uid[i] = decrypted[i];
		}
		
		
		
		int iuid = BitTools.byteArrayToInt(uid);
		int col = (int)decrypted[4] & 0xFF;
		int row = (int)decrypted[5] & 0xFF;
		System.out.println("\nAttempting to pick up item with uid: " + iuid + " at row: " + row + " col: " + col);
		int itemID = WMap.getInstance().getItem(Integer.valueOf(iuid)).getItem().getId();
		ItemFrame itams = (ItemFrame)cache.getItem(itemID);
		if(itams != null) {
			cur.getInventory().addItem(col, row, itams);
		} else {
			System.out.println("Halp. Items be null");		
		}
		
		byte[] itid = BitTools.intToByteArray(itemID);  
		
		pick[0] = (byte)pick.length;
		pick[4] = (byte)0x04;
		pick[6] = (byte)0x0F;
		
		byte[] iv = new byte[20];
		iv[0] = (byte)iv.length;
		iv[4] = (byte)0x05;
		iv[6] = (byte)0x0F;
		iv[8] = (byte)0x01;
		
		
		for(int i=0;i<4;i++) {
			pick[12+i] = chid[i];
			pick[28+i] = uid[i];
			pick[32+i] = itid[i];
			iv[16+i] = uid[i];
		}
		
		pick[24] = (byte)decrypted[4];
		pick[25] = (byte)decrypted[5]; 
		pick[26] = (byte)decrypted[6];
		pick[30] = (byte)decrypted[5];
		pick[31] = (byte)decrypted[6];
		pick[36] = (byte)0x01;
		pick[8]  = (byte)0x01;
		pick[9]  = (byte)0x9C;
		pick[10] = (byte)0x04;
		pick[11] = (byte)0x08;
		pick[16] = (byte)0x01;
		pick[18] = (byte)0xCF;
		pick[19] = (byte)0x2D;
		pick[20] = (byte)0x03;
		
		con.addWrite(iv);
		
		WMap.getInstance().removeItem(Integer.valueOf(iuid));
		
		return pick;
	}

}
