package GameServer.GamePackets;

import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.List;


import Connections.Connection;
import Database.CharacterDAO;
import Player.Character;
import Player.Player;
import Player.PlayerConnection;
import Tools.BitTools;

public class ReturnToSelection implements Packet {

	@Override
	public void execute(ByteBuffer buff) {
		// TODO Auto-generated method stub
		
	}

	public byte[] returnWritableByteBuffer(byte[] buffyTheVampireSlayer, Connection con) {
		Character current = ((PlayerConnection)con).getActiveCharacter();
		current.leaveGameWorld(); //leave the gameworld
		con.getWriteBuffer().clear(); //clear all packets pending write(prevent client from crashing as it returns to selection)
		Player tmplayer = ((PlayerConnection)con).getPlayer();
		
		tmplayer.setActiveCharacter(null);
		List<Character> characters = tmplayer.getCharacters();
		Iterator<Character> citer = characters.iterator();
		ByteBuffer all = ByteBuffer.allocate((characters.size()*649)+8+3);
		byte[] size = BitTools.shortToByteArray((short)all.capacity());
		all.put(size);
		all.put(new byte[] { (byte)0x00, (byte)0x00, (byte)0x03, (byte)0x00, (byte)0x01, (byte)0x00 }); //almost same header/packet as when logging in game(0x04 -> 0x01)
		
		all.put(new byte[] { (byte)0x01, (byte)0x01, (byte)0x01 });
		
		Character ctm = citer.next();
										
		byte[] tmp = ctm.initCharPacket();
		for(int i=0;i<tmp.length;i++) {
			all.put(tmp[i]);
		}
		
		while(citer.hasNext()) {

			Character ctmp = citer.next();
											
			byte[] tmpb = ctmp.initCharPacket();
			for(int i=0;i<tmpb.length;i++) {
				all.put(tmpb[i]);
			}
			
			all.put(10, (byte)((all.get(10)*2)+1)); //required increment depending on amount of characters on account
		}
		
		all.rewind();
		CharacterDAO.saveCharacter(current);
		byte[] rval = new byte[all.limit()];
		all.get(rval);
		return rval;
	}

}
