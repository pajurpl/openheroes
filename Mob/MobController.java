package Mob;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;

import World.OutOfGridException;
import World.Waypoint;

import logging.ServerLogger;

import Database.MobDAO;

public class MobController implements Runnable {
	private Map<Integer, Mob> mobs = Collections.synchronizedMap(new HashMap<Integer, Mob>());
	private int mobID, mobCount, uidPool;
	private int map, spawnx, spawny, spawnRadius,wpCount,wpHop, respawnTime;
	private volatile boolean active;
	private MobData data;
	private ServerLogger log = ServerLogger.getInstance();
	private MobTickRate ticks = new MobTickRate(10);
	private LinkedBlockingQueue<Mob> activeMobs = new LinkedBlockingQueue<Mob>();
	
	
	public MobController(int ID, int Count, int Pool, int []data) {
		this.mobID = ID;
		this.mobCount = Count;
		this.uidPool = Pool;
		this.map = data[0];
		this.spawnx = data[1];
		this.spawny = data[2];
		this.spawnRadius = data[3];
		this.wpCount = data[4];
		this.wpHop = data[5];
		this.respawnTime = data[6];
		this.setActive(false);
		
		this.init();
	}
	private void init(){
		this.data = MobDAO.getMobData(mobID);
		this.data.setGridID(map);
		this.data.setWaypointCount(wpCount);
		this.data.setWaypointHop(wpHop);
		this.data.setRespawnTime(respawnTime);
		Random r = new Random();
		Waypoint spawn;
		Mob mob = null;
		int uid = uidPool;
		int x,y;
		this.log.info(this, "Creating mob objects ");
		for (int i=0; i < this.mobCount; i++){
			// randomize spawn coordinates 
			x = this.spawnx + r.nextInt(2*this.spawnRadius) - this.spawnRadius;
			y = this.spawny + r.nextInt(2*this.spawnRadius) - this.spawnRadius;
			spawn = new Waypoint(x,y);
			mob = new Mob(this.mobID, uid, spawn, this);
			try {
				mob.run();
			} catch (OutOfGridException e) {
				this.log.severe(this, e.getMessage() + " Removing mob: " + uid + " in map: " + this.map);
				continue;
			}
			mobs.put(i, mob);
			this.ticks.addMob(mob);
			uid++;
		}
		this.log.info(this, this.mobCount + " mobs succesfully created");
	}
	public boolean isActive() {
		return active;
	}
	private void setActive(boolean active) {
		this.active = active;
	}
	
	public synchronized void run(){
		this.setActive(true);
		this.log.info(this, "Controller active in thread " + Thread.currentThread());
		
		while(this.active) {
			if(!this.activeMobs.isEmpty()) {
				List<Mob> cycle = this.ticks.getNextMobs();
				Iterator<Mob> it = cycle.iterator();
				Mob m = null;
				while(it.hasNext()) {
					m = it.next();
					try {
						m.run();
					} catch (OutOfGridException e) {
						this.log.severe(this, e.getMessage() + " Removing mob: " + m.getuid() + " in map: " + this.map);
						this.mobs.remove(m.getuid());
						it.remove();
					}
				}
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}	
			} else {
				this.setActive(false);
			}
		}
		this.log.info(this, "Controller deactivated in thread " + Thread.currentThread());
	}
	
	protected MobData getData(){
		return this.data;
	}
	
	protected void register(Mob mob) {
		this.activeMobs.offer(mob);
	}
	
	protected void unregister(Mob mob) {
		this.activeMobs.remove(mob);
	}
	
}
