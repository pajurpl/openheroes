package Mob;


import Tools.BitTools;
import World.Waypoint;

/*
 * MobPackets.class
 * Generates the necessary packets for the mobs
 */

public class MobPackets {
	
	public static byte[] getMovePacket(int uid, float x, float y){
		byte[] moveBucket = new byte[48];
		byte[] uniqueID = BitTools.intToByteArray(uid);
		byte[] moveX = BitTools.floatToByteArray(x);
		byte[] moveY = BitTools.floatToByteArray(y);
		
		moveBucket[0] = (byte)moveBucket.length;
		moveBucket[4] = (byte)0x05;
		moveBucket[6] = (byte)0x0D;
		moveBucket[8] =  (byte)0x02;
		moveBucket[9] =  (byte)0x10;
		moveBucket[10] = (byte)0xa0; 
		moveBucket[11] = (byte)0x36;

		
		for(int i=0;i<4;i++) {
			moveBucket[i+12] = uniqueID[i];
			moveBucket[i+20] = moveX[i];
			moveBucket[i+24] = moveY[i];
			moveBucket[i+28] = moveX[i];
			moveBucket[i+32] = moveY[i];
		}
		
		return moveBucket;
	}
	public static byte[] getDeathPacket(int uid, int id) {
		byte[] rval = new byte[20];
		
		rval[0] = 0x14;
		rval[4] = 0x05;
		rval[6] = 0x0a;
		
				
		byte[] bytes = BitTools.intToByteArray(uid);
		byte[] bid = new byte[] {0x02, 0x61, 0x21, 0x35}; //This might be some kind of death animation ID. Fixed value seems to work(must not be null)
		
		for(int i=0;i<4;i++) {
			rval[i+12] = bytes[i];
			rval[i+8] = bid[i];
		}
		
		return rval;
	}
	//public static ByteBuffer getInitialPacket(int mobID, int uid, float x, float y) {
	public static byte[] getInitialPacket(int mobID, int uid, Waypoint wp, int curhp) {
        byte[] mobBucket = new byte[608];
        byte[] size = BitTools.shortToByteArray((short)mobBucket.length);
       
        byte[] mobid = BitTools.shortToByteArray((short)mobID);
        byte[] mobUid = BitTools.intToByteArray(uid);
        byte[] xCoords = BitTools.floatToByteArray(wp.getX());
        byte[] yCoords = BitTools.floatToByteArray(wp.getY());
        byte[] hp = BitTools.intToByteArray(curhp);
        
        for(int i=0;i<2;i++) {
                mobBucket[i] = size[i];
                mobBucket[i+64] = mobid[i];
        }
        for(int i=0;i<4;i++) {
                mobBucket[i+12] = mobUid[i];
                mobBucket[i+84] = xCoords[i];
                mobBucket[i+88] = yCoords[i];
                mobBucket[i+72] = hp[i];
        }
       
        mobBucket[4] = (byte)0x05;
        mobBucket[6] = (byte)0x03;
        mobBucket[8] = (byte)0x02;
        
       
        return mobBucket;
	}

}
